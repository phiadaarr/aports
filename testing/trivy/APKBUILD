# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=trivy
pkgver=0.46.0
pkgrel=0
pkgdesc="Simple and comprehensive vulnerability scanner for containers"
url="https://github.com/aquasecurity/trivy"
arch="all"
# s390x: tests SIGSEGV: https://github.com/aquasecurity/trivy/issues/430
arch="$arch !s390x"
license="Apache-2.0"
makedepends="btrfs-progs-dev go linux-headers lvm2-dev"
source="https://github.com/aquasecurity/trivy/archive/v$pkgver/trivy-$pkgver.tar.gz"
options="net !check" # needs tinygo to turn go into wasm for tests

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	export CGO_ENABLED=0

	go build -o trivy -ldflags "-X main.version=$pkgver" cmd/trivy/main.go
}

check() {
	go test ./...
}

package() {
	install -Dm755 trivy -t "$pkgdir"/usr/bin/
}

sha512sums="
1ae082a35332e9be85dc461b0a71ac0c8a4d1d238af6099c8b721a20f75300e74eff9f3198638afb975e5dd859b03717954484883ed9bf780081d793b291bc85  trivy-0.46.0.tar.gz
"
