# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=mitra
pkgver=2.1.0
pkgrel=0
pkgdesc="ActivityPub microblogging platform written in Rust"
url="https://mitra.social/@mitra"
# riscv64: vite webapp fails to build: 'Parse error @:1:38'
arch="all !riscv64"
license="AGPL-3.0-only"
depends="postgresql"
makedepends="
	cargo
	cargo-auditable
	nodejs
	npm
	openssl-dev
	"
install="$pkgname.pre-install $pkgname.post-install"
pkgusers="mitra"
pkggroups="mitra"
subpackages="$pkgname-doc $pkgname-openrc"
source="mitra-$pkgver.tar.gz::https://codeberg.org/silverpill/mitra/archive/v$pkgver.tar.gz
	mitra-web-$pkgver.tar.gz::https://codeberg.org/silverpill/mitra-web/archive/v$pkgver.tar.gz
	mitra.initd
	init.sql
	config.yaml
	"
builddir="$srcdir/mitra"

# use system openssl
export OPENSSL_NO_VENDOR=1

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked

	cd "$srcdir/mitra-web"

	npm ci --foreground-scripts
}

build() {
	cargo auditable build --frozen --release \
		--features production

	cd "$srcdir/mitra-web"

	echo 'VITE_BACKEND_URL=' > .env.local
	npm run build
}

check() {
	# These tests require a database connection
	cargo test --frozen --workspace \
		--exclude mitra-models -- \
		--skip test_follow_unfollow \
		--skip test_hide_reblogs \
		--skip test_subscribe_unsubscribe \
		--skip test_get_jrd

	cd "$srcdir/mitra-web"

	npm run test
}

package() {
	install -Dm755 target/release/mitra -t "$pkgdir"/usr/bin
	install -Dm755 target/release/mitractl -t "$pkgdir"/usr/bin

	mkdir -p "$pkgdir"/usr/share/webapps
	cp -r "$srcdir"/mitra-web/dist \
		"$pkgdir"/usr/share/webapps/mitra

	install -Dm644 docs/* -t "$pkgdir"/usr/share/doc/$pkgname
	install -Dm644 config.yaml.example \
		contrib/Caddyfile contrib/*.nginx \
		contrib/monero/wallet.conf \
		-t "$pkgdir"/usr/share/doc/$pkgname/examples

	install -Dm640 -g mitra "$srcdir"/config.yaml -t "$pkgdir"/etc/mitra
	install -dm755 -o mitra -g mitra "$pkgdir"/var/lib/mitra
	install -Dm644 "$srcdir"/init.sql -t "$pkgdir"/var/lib/mitra
	install -Dm755 "$srcdir"/mitra.initd "$pkgdir"/etc/init.d/mitra
}

sha512sums="
1927ea43d39a02ca83a8f7570de91c3457a1e30f0cc7533c4a1db2ddb665aff5acf22da31a14015c8d5a2a1a2f1102c724e30d042b0f89fb905bed8645826a03  mitra-2.1.0.tar.gz
b78103fd664ae4b9484c31c6b781ef1c18f735600c6ac518a102594359d4d28fb2e7676c560a8a5599ec8d84f359b83209393a901ab2b9a4f7e38945cc8575a6  mitra-web-2.1.0.tar.gz
691f84f5dfdddc176e75792ab03ff167054246e75ced51be47a89f405ae55ebe5eb6280b73c1b467b5ecbe8539f6108fb3d86873d50fcc4f4b8c5b182632acb0  mitra.initd
02a40bf9dee625bf4b4783aa3588428313efa1e4d5ec5b8e64d76b9c6248f4d72b9d1287fba72b9d5d62c34352131d5d1609453583e257c5e8a1d2a787779147  init.sql
7988e0b305387712e74c32282b85e13a68abe14fe4ddbb31fe4612d9bdb2a33f3e867e53935809fe4147068f3344b0602df5b7930ba3ed436b27a9219eab9bd3  config.yaml
"
