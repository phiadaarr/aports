# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=certbot-nginx
pkgver=2.7.1
pkgrel=0
pkgdesc="Nginx plugin for Certbot client"
url="https://github.com/certbot/certbot"
arch="noarch"
license="Apache-2.0"
depends="
	certbot
	py3-acme
	py3-openssl
	py3-parsing
	py3-setuptools
	"
makedepends="
	py3-gpep517
	py3-wheel
	"
checkdepends="py3-pytest-xdist"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/certbot/certbot/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/certbot-$pkgver/$pkgname"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto -p no:warnings
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
01b4ca4455408bffddb6d8c49fc97a72692995a9c7e887ef3b4b259e1ebb11f5e3651ebb9a53b0a30c81457b3c49c28b5250493070c2c3cb3caa5ed4f707d149  certbot-nginx-2.7.1.tar.gz
"
