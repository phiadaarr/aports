# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kmenuedit
pkgver=5.27.8
pkgrel=1
pkgdesc="KDE menu editor"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kdbusaddons5-dev
	kdoctools5-dev
	kglobalaccel5-dev
	ki18n5-dev
	kiconthemes5-dev
	kinit5-dev
	kio5-dev
	kitemviews5-dev
	kxmlgui5-dev
	qt5-qtbase-dev
	samurai
	sonnet5-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kmenuedit.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kmenuedit-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7e28aeef137a185ecd54b6c5f1bbcc2e5fa4d304bd3d60468b68c96b6737312a7b1083b8b99b358299d04569d1db3fa7f00e68c3bc61bb6742d77eabca809791  kmenuedit-5.27.8.tar.xz
"
