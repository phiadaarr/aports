# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=lagrange
pkgver=1.17.0
pkgrel=0
pkgdesc="Beautiful Gemini client"
url="https://gmi.skyjake.fi/lagrange"
license="BSD-2-Clause"
arch="all"
makedepends="
	cmake
	fribidi-dev
	harfbuzz-dev
	libunistring-dev
	libwebp-dev
	mpg123-dev
	openssl-dev
	pcre2-dev
	samurai
	sdl2-dev
	zip
	zlib-dev
	"
subpackages="$pkgname-dbg $pkgname-doc"
source="https://git.skyjake.fi/gemini/lagrange/releases/download/v$pkgver/lagrange-$pkgver.tar.gz"
options="!check" # no test suite

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DENABLE_POPUP_MENUS=OFF \
		-DENABLE_RESIZE_DRAW=OFF \
		-DENABLE_X11_XLIB=OFF \
		-DTFDN_ENABLE_SSE41=OFF
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2e94e8696604badf255dc6c4a640a6ae4058a03f98da77bd5279381fec6c911fe3429ff95a2f6ae704047c507e2ef974bb40cfc35a1accb0a9941f64e43db817  lagrange-1.17.0.tar.gz
"
